<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Web extends CI_Controller {

	public function __construct(){
		parent:: __construct();
	}

	public function index()
	{	
		$css_custom = '';

		$js_custom = '';

		$array = array(
			'css_custom' => $css_custom,
			'js_custom' => $js_custom
		);

		$vista = $this->load->view('inicio/inicio',$array,TRUE);

		$this->getTemplate($vista, $array);
	}

	public function directorio()
	{	
		$css_custom = $js_custom = '';

		$array = array(
			'css_custom' => $css_custom,
			'js_custom' => $js_custom
		);

		$vista = $this->load->view('web/directorio',$array,TRUE);

		$this->getTemplate($vista, $array);
	}

		public function quienes_somos()
	{	
		$css_custom = $js_custom = '';

		$array = array(
			'css_custom' => $css_custom,
			'js_custom' => $js_custom
		);

		$vista = $this->load->view('web/quienes_somos',$array,TRUE);

		$this->getTemplate($vista, $array);
	}

	public function getTemplate($view, $array){

		$data = array(
				'header' => $this->load->view('layouts/header', $array, TRUE),
				'navbar' => $this->load->view('layouts/navbar', $array, TRUE),
				'slider' => $this->load->view('layouts/slider', $array, TRUE),
				'content' => $view,
				'footer' => $this->load->view('layouts/footer', $array, TRUE)
		);

		$this->load->view('layouts/page', $data);
	}

}

?>
