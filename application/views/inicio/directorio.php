<?php 

$directorio = array( array('img' => 'assets/style/images/art/team1.jpg' , 
                          'nombre' => 'Sr. Alcalde',
                          'cargo' => 'Alcalde' ),
                    array('img' => 'assets/style/images/art/team1.jpg' , 
                          'nombre' => 'Sr. Alcalde',
                          'cargo' => 'Alcalde' ),
                    array('img' => 'assets/style/images/art/team1.jpg' , 
                          'nombre' => 'Sr. Alcalde',
                          'cargo' => 'Alcalde' )
                           );

?>


<div class="light-wrapper">
  <div class="container inner">
    <h3 class="section-title text-center">Directorio <span> a tu servicio </span></h3>
    <div class="row">

      <?php foreach ($directorio as $key => $value) { ?>
      <div class="col-sm-4">
        <figure><img src="<?= base_url($value['img']) ?>" alt="" /></figure>
        <div class="bordered no-top-border text-center member">
          <h3><?= base_url($value['nombre']) ?> <span class="meta"><?= base_url($value['cargo']) ?></span></h3>
          <ul class="social">
            <li><a href="#"><i class="icon-s-twitter"></i></a></li>
            <li><a href="#"><i class="icon-s-facebook"></i></a></li>
            <li><a href="#"><i class="icon-s-dribbble"></i></a></li>
            <li><a href="#"><i class="icon-s-pinterest"></i></a></li>
          </ul>
        </div>
      </div>
      <?php }  ?>

    </div>
  </div>
</div>
<div id="first" class="parallax">
  <div class="inner">
    <h3 class="text-center section-title">About Us <span>Company Biography</span></h3>
    <p class="lead">Morbi leo risus, porta ac consectetur ac, vestibulum at eros. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Cras justo odio, dapibus facilisis egestas eget quam.</p>
    <p>Vivamus sagittis lacus vel augue laoreet rutrum faucibus dolor auctor. Vestibulum ligula porta felis euismod semper. Cras justo odio, dapibus ac facilisis in, egestas eget quam. Curabitur blandit tempus porttitor. Vestibulum ligula porta felis euismod semper. Donec id elit non mi porta gravida at eget metus. Duis mollis, est non commodo luctus. Morbi leo risus, porta ac consectetur, vestibulum at eros. Vivamus sagittis lacus vel augue laoreet rutrum faucibus dolor auctor. Duis mollis, est non commodo. Sed posuere consectetur est at lobortis. Maecenas sed diam eget risus varius blandit sit.</p>
  </div>
</div>