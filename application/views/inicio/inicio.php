<?php

$links = array( array('url'=>'#' , 'img'=>'assets/img/iconos/arte.png' , 'title'=>'Galeria'),
                      array('url'=>'#' , 'img'=>'assets/img/iconos/documento.png' , 'title'=>'Normas'),
                      array('url'=>'#' , 'img'=>'assets/img/iconos/obrero.png' , 'title'=>'Proyectos'),
                      array('url'=>'#' , 'img'=>'assets/img/iconos/calendario.png' , 'title'=>'Eventos'),                     
                      array('url'=>'#' , 'img'=>'assets/img/iconos/s-contraloria.png' , 'title'=>'Contraloria')
                  );

$noticias = array( array('id' => 1, 'titulo' => 'Municipalidad de Huicungo', 'url' => '', 'texto' => ''),
                    array('id' => 2,'titulo' => 'Asfalto de la calle Ayacucho', 'url' =>'' , 'texto' =>'' ),
                    array('id' => 3,'titulo' => 'Apertura de Vaso de Leche', 'url' => '', 'texto' =>'' ),
                    array('id' => 4,'titulo' => 'Inicio de Año escolar 2020', 'url' => '', 'texto' =>'' )
                  );

?>

<div class="blog no-sidebar">
  <div class="light-wrapper">
    <div class="container inner">
      <h3 class="filter-title text-center">Accesos Rapidos</h3>
      <ul class="format-filter text-center">

        <?php foreach ($links as $key => $value) { ?>
          <li>
            <a class="active" href="<?= $value['url']?>" data-filter="*" title="<?= $value['title']?>" data-rel="tooltip" data-placement="top">
            <img width="115" height="106" src="<?= base_url($value['img']) ?>" alt="Imagen link"></a>
          </li>
        <?php } ?>
       
      </ul>
    </div>
  </div>

  <div class="dark-wrapper inner">

    <div class="container ">
      <div class="row">
        <div class="col-sm-8">
          <h3 class="section-title text-left">Noticias <span> mantente informado</span></h3>
            <div class="tabs tabs-side tab-container">
              <ul class="etabs" style="width: 30%">
                <?php foreach ($noticias as $key => $value) { ?>
                <li class="tab"><a href="#tab<?=$value['id']?>"><?= $value['titulo']?></a></li>
                <? } ?>
              </ul>

              <div class="panel-container" style="width: 70%">

                <?php foreach ($noticias as $key => $value) { ?>
                <div class="tab-block" id="tab<?=$value['id']?>">
                  <h4>Municipalidad de Huicungo</h4>
                  <div class="divide5"></div>
                  <img src="assets/img/logo_1.png" alt="" class="pull-left rm20" style="width: 100%" />
                  <p>Para mejorar en la difusión de la información, viene trabajando en la construcción de una plataforma web, que facilite la disponibilidad y accesibilidad. </p>
                </div>
                <?php }  ?>
              </div>
            </div>

           
            
        </div>
        <div class="col-sm-4">   
          <h3 class="section-title text-left">Redes Sociales <span> siguenos</span></h3> 
          <div class="divide5"></div>
          <iframe src="https://www.facebook.com/plugins/page.php?href=https%3A%2F%2Fweb.facebook.com%2Fmdhinfor%2F&tabs=timeline&width=340&height=500&small_header=false&adapt_container_width=true&hide_cover=false&show_facepile=true&appId=1234404519917423" width="340" height="500" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowTransparency="true" allow="encrypted-media"></iframe>      
        </div>
      </div>

    </div>

  </div>
  

  
</div>