<div class="light-wrapper">
  <div class="container inner">
    <h3 class="section-title text-center">Quienes Somos <span> Nuestros Objetivos y metas</span></h3>
    <ul class="row-fluid process">
      <li class="col-sm-6 process-item border-right border-bottom"> <span class="number">01</span>
        <div class="info">
          <h4>Misión</h4>
          <p>Promover el desarrollo integral - sostenible de los recursos y servicios públicos a través de la calidad y una adecuada prestación de servicios a la población; en el marco de una gestión eficiente, transparente y participativa.</p>
          <ul class="lp0 circled">
            <li>Transparencia.</li>
            <li>Calidad.</li>
          </ul>
        </div>
      </li>
      <li class="col-sm-6 process-item border-bottom"> <span class="number">02</span>
        <div class="info">
          <h4>Visión</h4>
          <p>Provincia de San Martín, con bienestar social e igualdad de oportunidades, impulsor de progreso integral de sus pueblos, ejemplo de competitividad y desarrollo sostenible.</p>
          
        </div>
      </li>
      <li class="col-sm-6 process-item border-right"> <span class="number">03</span>
        <div class="info">
          <h4>Valores</h4>
          <p>Siempre buscando.</p>
          <ul class="lp0 circled">
            <li>Justicia.</li>
            <li>Respeto.</li>
            <li>Inclusión.</li>
          </ul>
        </div>
      </li>
    </ul>
  </div>
</div>