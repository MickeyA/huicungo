<?php 

$directorio = array( array('img' => 'assets/style/images/art/team1.jpg' , 
                          'nombre' => 'Sr. Alcalde',
                          'cargo' => 'Alcalde' ),
                    array('img' => 'assets/style/images/art/team1.jpg' , 
                          'nombre' => 'Sr. Alcalde',
                          'cargo' => 'Alcalde' ),
                    array('img' => 'assets/style/images/art/team1.jpg' , 
                          'nombre' => 'Sr. Alcalde',
                          'cargo' => 'Alcalde' ),
                    array('img' => 'assets/style/images/art/team1.jpg' , 
                          'nombre' => 'Sr. Alcalde',
                          'cargo' => 'Alcalde' ),
                    array('img' => 'assets/style/images/art/team1.jpg' , 
                          'nombre' => 'Sr. Alcalde',
                          'cargo' => 'Alcalde' )
                           );

?>


<div class="light-wrapper">
  <div class="container inner">
    <h3 class="section-title text-center">Directorio <span> a tu servicio </span></h3>
    <div class="row">

      <?php foreach ($directorio as $key => $value) { ?>
      <div class="col-sm-4">
        <figure><img src="<?= base_url($value['img']) ?>" alt="" /></figure>
        <div class="bordered no-top-border text-center member">
          <h3><?= ($value['nombre']) ?> <span class="meta"><?= ($value['cargo']) ?></span></h3>
          <ul class="social">
            <li><a href="#"><i class="icon-s-twitter"></i></a></li>
            <li><a href="#"><i class="icon-s-facebook"></i></a></li>
            <li><a href="#"><i class="icon-s-dribbble"></i></a></li>
            <li><a href="#"><i class="icon-s-pinterest"></i></a></li>
          </ul>
        </div>
      </div>
      <?php }  ?>

    </div>
  </div>
</div>
<div id="first" class="parallax">
  <div class="inner">
    <h3 class="text-center section-title">Nosotros <span> Somos tu Municipio</span></h3>
    <p class="lead">La municipalidad de Huicungo.</p>
    <p>La municipalidad, siempre velando por sus ciudadanos.</p>
  </div>
</div>