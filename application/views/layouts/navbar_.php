<?php 
$menu_usuario = array( array( 'url'=>'col' , 'icono'=>'fa', 'nombre'=>'inicio','papa'=>'inicio' ) ,
                     array( 'url'=>'col' , 'icono'=>'fa', 'nombre'=>'inicio','papa'=>'inicio' ) ,
                     array( 'url'=>'col' , 'icono'=>'fa', 'nombre'=>'inicio','papa'=>'servicios' ) 
                    );

$modulo_papa_flag = 1;
$menu = '';    


foreach ( $menu_usuario as $modulo) {

    if( $modulo_papa_flag != $modulo['papa'] ){

        if($modulo_papa_flag != '' ){
                $menu .="</ul> </li>";                            
        }

        $menu .= "<li class='dropdown'>
          <a href='#' class='dropdown-toggle js-activated'>
             {$modulo['papa']} 
          </a>
          <ul class='dropdown-menu'>";
        $modulo_papa_flag = $modulo['papa'];   

    }
    $menu .=  "<li><a href='".base_url($modulo['url'])."'><i class='fa {$modulo['icono']}'></i> {$modulo['nombre']}</a></li>"; 

}

// $menu .= "</li>";

// die($menu);


?>

<div class="yamm navbar">
  <div class="navbar-header">
    <div class="container"> <a class="btn responsive-menu pull-right" data-toggle="collapse" data-target=".navbar-collapse"><i class='icon-menu-1'></i></a> <a class="navbar-brand" href="<?php echo base_url()?>"><img width="185" height="55" src="<?php echo base_url()?>assets/img/logo_1.png" alt="" /></a>
      <ul class="info pull-right">
        <li><i class="icon-phone-1"></i> 0535 954 74 54</li>
        <li><i class="icon-mail-alt"></i> <a href="mailto:email@moose.com">email@moose.com</a></li>
      </ul>
    </div>
  </div>
  <div class="collapse navbar-collapse">
    <div class="container">
      <ul class="nav navbar-nav">
        <!-- <?= $menu ?> -->
        <!-- <li class="dropdown"><a href="#" class="dropdown-toggle js-activated">Home</a>
          <ul class="dropdown-menu">
            <li><a href="index.html">Home Layout 1</a></li>
            <li><a href="index2.html">Home Layout 2</a></li>
            <li><a href="index3.html">Home Layout 3</a></li>
            <li><a href="index4.html">Home Layout 4</a></li>
            <li><a href="index5.html">Home Layout 5</a></li>
            <li><a href="index6.html">Home Layout 6</a></li>
            <li><a href="index7.html">Home Layout 7</a></li>
            <li><a href="index8.html">Home Layout 8</a></li>
            <li><a href="index9.html">Home Layout 9</a></li>
            <li><a href="index10.html">Home Layout 10</a></li>
            <li><a href="index11.html">Home Layout 11</a></li>
            <li><a href="index12.html">Home Layout 12</a></li>
            <li><a href="index13.html">Home Layout 13</a></li>
            <li><a href="index14.html">Home Layout 14</a></li>
          </ul>
        </li>
        <li class="dropdown"><a href="#" class="dropdown-toggle js-activated">Portfolio</a>
          <ul class="dropdown-menu">
            <li><a href="portfolio.html">Grid 4 Columns</a></li>
            <li><a href="portfolio2.html">Grid 3 Columns</a></li>
            <li><a href="portfolio3.html">Fullscreen Grid</a></li>
            <li><a href="portfolio4.html">Details Grid 4 Columns</a></li>
            <li><a href="portfolio5.html">Details Grid 3 Columns</a></li>
            <li><a href="portfolio-post.html">Post with Wide Slider</a></li>
            <li><a href="portfolio-post2.html">Post with Half Slider</a></li>
            <li><a href="portfolio-post3.html">Post with Gallery</a></li>
            <li><a href="portfolio-post4.html">Post with Images</a></li>
            <li><a href="portfolio-post5.html">Post with Video</a></li>
            <li><a href="portfolio-post6.html">Post with Audio</a></li>
            <li><a href="portfolio-post-parallax.html">Post with Parallax</a></li>
          </ul>
        </li>
        <li class="dropdown"><a href="#" class="dropdown-toggle js-activated">Blog</a>
          <ul class="dropdown-menu">
            <li><a href="blog.html">Blog with Sidebar</a></li>
            <li><a href="blog2.html">Blog without Sidebar</a></li>
            <li><a href="blog3.html">Grid Blog 3 Columns</a></li>
            <li><a href="blog4.html">Grid Blog 2 Columns</a></li>
            <li><a href="blog5.html">Grid Blog with Sidebar</a></li>
            <li><a href="blog-post.html">Post with Sidebar</a></li>
            <li><a href="blog2-post.html">Post without Sidebar</a></li>
            <li><a href="blog3-post.html">Post with Sidebar II</a></li>
          </ul>
        </li>
        <li class="dropdown"><a href="#" class="dropdown-toggle js-activated">Pages</a>
          <ul class="dropdown-menu">
            <li><a href="about.html">About</a></li>
            <li><a href="services.html">Services 1</a></li>
            <li><a href="services2.html">Services 2</a></li>
            <li><a href="services3.html">Services 3</a></li>
            <li><a href="faq.html">FAQ 1</a></li>
            <li><a href="faq2.html">FAQ 2</a></li>
            <li><a href="login.html">Login Page</a></li>
            <li><a href="pages.html">Side Navigation</a></li>
            <li><a href="pricing.html">Pricing</a></li>
          </ul>
        </li>
        <li class="dropdown"><a href="#" class="dropdown-toggle js-activated">Styles</a>
          <ul class="dropdown-menu">
            <li><a href="elements.html">Elements</a></li>
            <li><a href="headings.html">Headings</a></li>
          </ul>
        </li>
        <li class="dropdown"><a href="#" class="dropdown-toggle js-activated">Features</a>
          <ul class="dropdown-menu">
            <li class="dropdown-submenu"> <a href="title1.html">Page Titles</a>
              <ul class="dropdown-menu">
                <li><a href="title1.html">Title 1</a></li>
                <li><a href="title2.html">Title 2</a></li>
                <li><a href="title3.html">Title 3</a></li>
                <li><a href="title4.html">Title 4</a></li>
                <li><a href="portfolio-post-parallax.html">Title with Parallax</a></li>
              </ul>
            <li><a href="font-icons.html">Font Icons</a></li>
            <li class="dropdown-submenu"> <a href="index.html">Headers</a>
              <ul class="dropdown-menu">
                <li><a href="index.html">Header Style 1</a></li>
                <li><a href="header2.html">Header Style 2</a></li>
              </ul>
            </li>
            <li class="dropdown-submenu"><a href="#">Colors</a>
              		<ul class="dropdown-menu">
		                <li><a class="changecolor" title="mooblue-color">Blue</a></li>
		                <li><a class="changecolor" title="moogray-color">Gray</a></li>
						<li><a class="changecolor" title="moogreen-color">Green</a></li>
						<li><a class="changecolor" title="moonavy-color">Navy</a></li>
						<li><a class="changecolor" title="mooorange-color">Orange</a></li>
						<li><a class="changecolor" title="moopink-color">Pink</a></li>
						<li><a class="changecolor" title="moopurple-color">Purple</a></li>
						<li><a class="changecolor" title="moored-color">Red</a></li>
		              </ul>
              	</li>
            <li class="dropdown-submenu"> <a href="#">Submenu Levels</a>
              <ul class="dropdown-menu">
                <li><a href="#">Second Level</a></li>
                <li class="dropdown-submenu"> <a href="#">More</a>
                  <ul class="dropdown-menu">
                    <li><a href="#">Third Level</a></li>
                    <li><a href="#">Third Level</a></li>
                  </ul>
                </li>
                <li><a href="#">Second Level</a></li>
                <li><a href="#">Second Level</a></li>
              </ul>
            </li>
          </ul>
        </li>
        <li class="dropdown yamm-fullwidth"><a href="#" class="dropdown-toggle js-activated">Mega Menu</a>
          <ul class="dropdown-menu yamm-dropdown-menu">
            <li>
              <div class="yamm-content row">
                <div class="col-sm-3">
                  <h4>Latest Works</h4>
                  <p>Cum sociis natoque penatibus magnis dis parturient montes, nascetur mus.</p>
                  <figure class="icon-overlay icn-link"><a href="portfolio-post.html"><img src="style/images/art/p1.jpg" alt="" /></a></figure> </div>
                <div class="col-sm-3">
                  <h4>About Us</h4>
                  <p>Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Cras justo odio, dapibus ac facilisis in, egestas eget quam. </p>
                  <p>Duis mollis, est non commodo luctus. Nullam quis risus eget urna mollis ornare vel eu leo cras consectetur.</p>
                  <div class="divide5"></div>
                  <a href="#" class="btn btn-blue">Read More</a> </div>
                <div class="col-sm-3">
                  <h4>Get In Touch</h4>
                  <div class="map">
                    <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3172.4564967931838!2d-122.03250858386237!3d37.33169997984258!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x808fb5b6e94ffe23%3A0x63bb79532baa6af4!2sApple+Campus!5e0!3m2!1sen!2str!4v1553850783971!5m2!1sen!2str" width="100%" height="180" frameborder="0" style="border:0" allowfullscreen></iframe>
                  </div>
                  <div class="divide20"></div>
                  <i class="icon-location contact"></i> Moonshine St. 14/05 Jupiter <br />
                  <i class="icon-phone contact"></i>+00 (123) 456 78 90 <br />
                  <i class="icon-mail contact"></i> <a href="first.last%40email.html">first.last@email.com</a> </div>
                <div class="col-sm-3">
                  <h4>Special Pages</h4>
                  <ul class="circled">
                    <li><a href="http://themes.iki-bir.com/moo/portfolio5.html">Detailed Grid Portfolio</a></li>
                    <li><a href="http://themes.iki-bir.com/moo/portfolio3.html">Fullscreen Grid Portfolio</a></li>
                    <li><a href="http://themes.iki-bir.com/moo/blog2.html">Blog without Sidebar</a></li>
                    <li><a href="http://themes.iki-bir.com/moo/services.html">Services Layout 1</a></li>
                    <li><a href="http://themes.iki-bir.com/moo/index.html">Home layout 1</a></li>
                    <li><a href="http://themes.iki-bir.com/moo/elements.html">Template Elements</a></li>
                    <li><a href="http://themes.iki-bir.com/moo/headings.html">Heading Styles</a></li>
                    <li><a href="http://themes.iki-bir.com/moo/login.html">Login Page</a></li>
                    <li><a href="http://themes.iki-bir.com/moo/pricing.html">Pricing Tables</a></li>
                  </ul>
                </div>
              </div>
            </li>
          </ul>
        </li> ->
        <li class="dropdown"><a href="#" class="dropdown-toggle js-activated">Contact</a>
          <ul class="dropdown-menu">
            <li><a href="contact.html">Contact 1</a></li>
            <li><a href="contact2.html">Contact 2</a></li>
            <li><a href="contact3.html">Contact 3</a></li>
          </ul>
        </li> -->
      </ul>
    </div>
  </div>
  <!--/.nav-collapse --> 
</div>