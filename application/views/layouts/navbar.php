<?php 
$menu_usuario = array(array( 'url'=>'web/quienes_somos' , 'icono'=>'fa', 'nombre'=>'Quienes Somos','papa'=>'inicio' ) ,
                     array( 'url'=>'col' , 'icono'=>'fa', 'nombre'=>'Empresas Municipales','papa'=>'inicio' ) ,
                     array( 'url'=>'web/directorio' , 'icono'=>'fa', 'nombre'=>'Directorio','papa'=>'inicio' ) ,
                     array( 'url'=>'col' , 'icono'=>'fa', 'nombre'=>'Galeria','papa'=>'inicio' ) ,

                     array( 'url'=>'col' , 'icono'=>'fa', 'nombre'=>'Biblioteca Municipal','papa'=>'servicios' ) ,
                     array( 'url'=>'col' , 'icono'=>'fa', 'nombre'=>'Defensa Civil','papa'=>'servicios' ) ,
                     array( 'url'=>'col' , 'icono'=>'fa', 'nombre'=>'Demuna','papa'=>'servicios' ),
                     array( 'url'=>'col' , 'icono'=>'fa', 'nombre'=>'Normas','papa'=>'Municipio' ),
                     array( 'url'=>'col' , 'icono'=>'fa', 'nombre'=>'Planes y Política','papa'=>'Municipio' ),
                     array( 'url'=>'col' , 'icono'=>'fa', 'nombre'=>'Obras','papa'=>'Obras y proyectos' ),
                     array( 'url'=>'col' , 'icono'=>'fa', 'nombre'=>'Proyectos ','papa'=>'Obras y proyectos' ) ,
                     array( 'url'=>'col' , 'icono'=>'fa', 'nombre'=>'Eventos ','papa'=>'Calendario' ) ,
                     array( 'url'=>'col' , 'icono'=>'fa', 'nombre'=>'Acontecimientos ','papa'=>'Calendario' ) ,

                     array( 'url'=>'col' , 'icono'=>'fa', 'nombre'=>'Contactarnos ','papa'=>'Contacto' ),
                     array( 'url'=>'col' , 'icono'=>'fa', 'nombre'=>'Redes Sociales ','papa'=>'Contacto' ) 
                    );

$modulo_papa_flag = '';
$menu = '';    


foreach ( $menu_usuario as $modulo) {

    if( $modulo_papa_flag != $modulo['papa'] ){

        if($modulo_papa_flag != '' ){
                $menu .="</ul> </li>";                            
        }

        $menu .= "<li class='dropdown'>
          <a href='#' class='dropdown-toggle js-activated'>
             {$modulo['papa']} 
          </a>
          <ul class='dropdown-menu'>";
        $modulo_papa_flag = $modulo['papa'];   

    }
    $menu .=  "<li><a href='".base_url($modulo['url'])."'><i class='fa {$modulo['icono']}'></i> {$modulo['nombre']}</a></li>"; 

}


?>

<div class="yamm navbar">
  <div class="navbar-header">
    <div class="container"> <a class="btn responsive-menu pull-right" data-toggle="collapse" data-target=".navbar-collapse"><i class='icon-menu-1'></i></a> <a class="navbar-brand" href="<?php echo base_url()?>"><img width="185" height="55" src="<?php echo base_url()?>assets/img/logo_1.png" alt="" /></a>
      <ul class="info pull-right">
        <li><i class="icon-phone-1"></i> 0535 954 74 54</li>
        <li><i class="icon-mail-alt"></i> <a href="mailto:email@moose.com">email@moose.com</a></li>
      </ul>
    </div>
  </div>
  <div class="collapse navbar-collapse">
    <div class="container">
      <ul class="nav navbar-nav">
        <?= $menu ?>
      </ul>
    </div>
  </div>
  <!--/.nav-collapse --> 
</div>