<div class="fullwidthbanner-container revolution">
  <div class="fullwidthbanner">
    <ul>
      <li data-transition="fade"> <img src="<?php echo base_url()?>assets/style/images/art/slider-bg1.jpg" alt="" />
        <!-- <div class="caption bold white-bg sfr" data-x="550" data-y="215" data-speed="500" data-start="2000" data-easing="Sine.easeOut">Think unique. Be unique.</div>
        <div class="caption lite white-bg sfr" data-x="550" data-y="280" data-speed="500" data-start="2500" data-easing="Sine.easeOut">Make a difference using Moose.</div>
        <div class="caption sfr" data-x="550" data-y="340" data-speed="500" data-start="3000" data-easing="Sine.easeOut"><a href="#" class="btn btn-large">Start Now</a></div> -->
      </li>
      <li data-transition="fade"> <img src="<?php echo base_url()?>assets/style/images/art/slider-bg2.jpg" alt="" />
        <!-- <div class="caption sft bold opacity-bg" data-x="620" data-y="205" data-speed="500" data-start="2000" data-easing="Sine.easeOut">Showcase your business</div>
        <div class="caption sfr bold opacity-bg" data-x="620" data-y="270" data-speed="500" data-start="2500" data-easing="Sine.easeOut">with editable PSD mockups</div>
        <div class="caption sfb" data-x="620" data-y="335" data-speed="500" data-start="3000" data-easing="Sine.easeOut"><a href="#" class="btn btn-large">Purchase Now</a></div> -->
      </li>
      <li data-transition="fade"> <img src="<?php echo base_url()?>assets/style/images/art/slider-bg3.jpg" alt="" />
        <!-- <div class="caption sfl bold opacity-bg" data-x="35" data-y="180" data-speed="500" data-start="1500" data-easing="Sine.easeOut">100% Responsive</div>
        <div class="caption sfr bold opacity-bg" data-x="35" data-y="245" data-speed="500" data-start="2000" data-easing="Sine.easeOut">Retina-ready design</div>
        <div class="caption sfb" data-x="35" data-y="310" data-speed="500" data-start="2500" data-easing="Sine.easeOut"><a href="#" class="btn btn-green btn-large">Get in Touch</a></div> -->
      </li>
      <li data-transition="fade"> <img src="<?php echo base_url()?>assets/style/images/art/slider-image22.jpg" alt="" />
        <!-- <div class="caption sft bold white-bg" data-x="center" data-y="215" data-speed="500" data-start="500" data-easing="Sine.easeOut">Moose brings awesomeness</div>
        <div class="caption sfb lite white-bg" data-x="center" data-y="281" data-speed="500" data-start="1000" data-easing="Sine.easeOut">A responsive template with clean and professional design</div>
        <div class="caption sfb" data-x="center" data-y="339" data-speed="500" data-start="1500" data-easing="Sine.easeOut"><a href="#" class="btn btn-large">Purchase Now</a></div> -->
      </li>
      <li data-transition="fade"> <img src="<?php echo base_url()?>assets/style/images/art/slider-bg5.jpg" alt="" />
        <!-- <div class="caption sft bold white-bg" data-x="center" data-y="215" data-speed="500" data-start="500" data-easing="Sine.easeOut">Moose brings awesomeness</div>
        <div class="caption sfb lite white-bg" data-x="center" data-y="281" data-speed="500" data-start="1000" data-easing="Sine.easeOut">A responsive template with clean and professional design</div>
        <div class="caption sfb" data-x="center" data-y="339" data-speed="500" data-start="1500" data-easing="Sine.easeOut"><a href="#" class="btn btn-large">Purchase Now</a></div> -->
      </li>
    </ul>
    <div class="tp-bannertimer tp-bottom"></div>
  </div>
  <!-- /.fullwidthbanner --> 
</div>