<!-- Mirrored from themes.iki-bir.com/moose/index12.html by HTTrack Website Copier/3.x [XR&CO'2014], Fri, 31 Jan 2020 21:27:00 GMT -->
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="description" content="">
<meta name="author" content="">
<link rel="shortcut icon" href="<?php echo base_url()?>assets/img//logo.jpeg">
<title>Municipalidad Distrital de Huicungo</title>

<!-- Bootstrap core CSS -->
<link href="<?php echo base_url()?>assets/style/css/bootstrap.css" rel="stylesheet">
<link href="<?php echo base_url()?>assets/style/css/settings.css" rel="stylesheet">
<link href="<?php echo base_url()?>assets/style/css/owl.carousel.css" rel="stylesheet">
<link href="<?php echo base_url()?>assets/style/js/google-code-prettify/prettify.css" rel="stylesheet">
<link href="<?php echo base_url()?>assets/style/js/fancybox/jquery.fancybox.css" rel="stylesheet" type="text/css" media="all" />
<link href="<?php echo base_url()?>assets/style/js/fancybox/helpers/jquery.fancybox-thumbs0ff5.css?v=1.0.2" rel="stylesheet" type="text/css" />
<link href="<?php echo base_url()?>assets/style.css" rel="stylesheet">
<link href="<?php echo base_url()?>assets/style/css/color/red.css" rel="stylesheet">
<link href='http://fonts.googleapis.com/css?family=Roboto:400,400italic,500,500italic,700italic,700,900,900italic,300italic,300,100italic,100' rel='stylesheet' type='text/css'>
<link href='http://fonts.googleapis.com/css?family=Roboto+Condensed:300italic,400italic,700italic,400,700,300' rel='stylesheet' type='text/css'>
<link href='http://fonts.googleapis.com/css?family=Roboto+Slab:400,700,300,100' rel='stylesheet' type='text/css'>
<link href="<?php echo base_url()?>assets/style/type/fontello.css" rel="stylesheet">

<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!--[if lt IE 9]>
      <script src="style/js/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
</head>