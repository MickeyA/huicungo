<footer class="black-wrapper">
  <div class="container inner">
    <div class="row">
      <section class="col-sm-3 widget">
        <div class="row"><a class="navbar-brand" href="http://localhost/huicungo/"><img width="185" height="55" src="http://localhost/huicungo/assets/img/logo_1.png" alt=""></a></div>
        <br>
        <h3 class="section-title widget-title">Huicungo - Perú</h3>
        <p>Trabajando.</p>
        <p>El Perú primero.</p>
        <p>Transparencia.</p>
      </section>

      <section class="col-sm-3 widget">
        
        <div class="row"><a class="navbar-brand" href="http://localhost/huicungo/"><img width="185" height="55" src="http://localhost/huicungo/assets/img/logo_1.png" alt=""></a></div>
        <h3 class="section-title widget-title">Perú</h3>
        <ul class="post-list">
          <li>
            <h6><a href="blog-post.html">Vivamus sagittis lacus vel augue metus</a></h6>
            <em>3th Oct 2012</em> </li>
          <li>
            <h6><a href="blog-post.html">Scelerisque nisl consectetur et</a></h6>
            <em>28th Sep 2012</em> </li>
          <li>
            <h6><a href="blog-post.html">Pellentesque ornare sem lacinia quam</a></h6>
            <em>15th Aug 2012</em> </li>
        </ul>
        <!-- /.post-list --> 
      </section>
      <!-- /.widget -->
      <section class="col-sm-3 widget">
        <h3 class="section-title widget-title">About</h3>
        <p>Aenean lacinia bibendum nulla sed leo posuere erat a ante venenatis dapibus posuere velit aliquet.</p>
        <p>Donec ullamcorper metus auctor fringi. Nullam quis risus eget.</p>
        <p>Vestibulum id ligula porta  euismod semper. Maecenas faucibus mollis.</p>
      </section>
      <!-- /.widget -->
      <section class="col-sm-3 widget">
        <h3 class="section-title widget-title">Tags</h3>
        <div class="tagcloud"> <a href="#" style="font-size: 9pt;">blogroll</a> <a href="#" style="font-size: 19pt;">daily</a> <a href="#" style="font-size: 9pt;">dialog</a> <a href="#" style="font-size: 9pt;">gallery</a> <a href="#" style="font-size: 10pt;">journal</a> <a href="#" style="font-size: 9pt;">link</a> <a href="#" style="font-size: 12pt;">motion</a> <a href="#" style="font-size: 9pt;">music</a> <a href="#" style="font-size: 20pt;">photo</a> <a href="#" style="font-size: 13pt;">professional</a> <a href="#" style="font-size: 16pt;">quotation</a> <a href="#" style="font-size: 9pt;">show</a> <a href="#" style="font-size: 15pt;">sound</a> <a href="#" style="font-size: 9pt;">tv</a> <a href="#" style="font-size: 9pt;">video</a> <a href="#" style="font-size: 9pt;">gift</a> <a href="#" style="font-size: 19pt;">label</a> <a href="#" style="font-size: 9pt;">christmas</a> <a href="#" style="font-size: 9pt;">holiday</a> <a href="#" style="font-size: 10pt;">fun</a> <a href="#" style="font-size: 9pt;">recipes</a> <a href="#" style="font-size: 12pt;">concert</a> <a href="#" style="font-size: 9pt;">drinks</a> <a href="#" style="font-size: 20pt;">apps</a> <a href="#" style="font-size: 13pt;">iphone</a> <a href="#" style="font-size: 16pt;">ipad</a> <a href="#" style="font-size: 9pt;">develop</a> <a href="#" style="font-size: 15pt;">marketing</a> <a href="#" style="font-size: 9pt;">strategy</a> <a href="#" style="font-size: 13pt;">food</a> <a href="#" style="font-size: 12pt;">typography</a> <a href="#" style="font-size: 9pt;">mobile</a> <a href="#" style="font-size: 15pt;">envato</a> <a href="#" style="font-size: 9pt;">icon</a></div>
      </section>
      <!-- /.widget -->
      <section class="col-sm-3 widget">
        <h3 class="section-title widget-title">Get In Touch</h3>
        <p>Fusce dapibus, tellus commodo, tortor mauris condimentum utellus fermentum, porta sem malesuada magna. Sed posuere consectetur est at lobortis.</p>
        <div class="divide10"></div>
        <i class="icon-location contact"></i> Moonshine St. 14/05 Light City, Jupiter <br />
        <i class="icon-phone contact"></i>+00 (123) 456 78 90 <br />
        <i class="icon-mail contact"></i> <a href="first.last%40email.html">first.last@email.com</a> </section>
      <!-- /.widget --> 
    </div>
    <!-- /.row --> 
  </div>
  <!-- .container -->
  
  <div class="sub-footer">
    <div class="container">
      <p class="pull-left">© 2020 Moose. All rights reserved. Theme by <a href="http://elemisfreebies.com/">elemis</a>.</p>
      <ul class="footer-menu pull-right">
        <li><a href="#">Home</a></li>
        <li><a href="#">About</a></li>
        <li><a href="#">Portfolio</a></li>
        <li><a href="#">Blog</a></li>
        <li><a href="#">Contact</a></li>
      </ul>
    </div>
  </div>
</footer>
<!-- /footer --> 

<!-- Bootstrap core JavaScript
    ================================================== --> 
<!-- Placed at the end of the document so the pages load faster --> 
<script src="<?php echo base_url()?>assets/style/js/jquery.min.js"></script> 
<script src="<?php echo base_url()?>assets/style/js/bootstrap.min.js"></script> 
<script src="<?php echo base_url()?>assets/style/js/twitter-bootstrap-hover-dropdown.min.js"></script> 
<script src="<?php echo base_url()?>assets/style/js/jquery.themepunch.plugins.min.js"></script> 
<script src="<?php echo base_url()?>assets/style/js/jquery.themepunch.revolution.min.js"></script> 
<script src="<?php echo base_url()?>assets/style/js/jquery.fancybox.pack.js"></script> 
<script src="<?php echo base_url()?>assets/style/js/fancybox/helpers/jquery.fancybox-thumbs0ff5.js?v=1.0.2"></script> 
<script src="<?php echo base_url()?>assets/style/js/fancybox/helpers/jquery.fancybox-mediae209.js?v=1.0.0"></script> 
<script src="<?php echo base_url()?>assets/style/js/jquery.isotope.min.js"></script> 
<script src="<?php echo base_url()?>assets/style/js/jquery.easytabs.min.js"></script> 
<script src="<?php echo base_url()?>assets/style/js/owl.carousel.min.js"></script> 
<script src="<?php echo base_url()?>assets/style/js/jquery.fitvids.js"></script> 
<script src="<?php echo base_url()?>assets/style/js/jquery.sticky.js"></script> 
<script src="<?php echo base_url()?>assets/style/js/google-code-prettify/prettify.js"></script> 
<script src="<?php echo base_url()?>assets/style/js/jquery.slickforms.js"></script> 
<script src="<?php echo base_url()?>assets/style/js/scripts.js"></script>
<!-- DEMO ONLY -->
<link rel="alternate stylesheet" type="text/css" href="<?php echo base_url()?>assets/switcher/blue.css" title="mooblue-color" media="screen" />
<link rel="alternate stylesheet" type="text/css" href="<?php echo base_url()?>assets/switcher/gray.css" title="moogray-color" media="screen" />
<link rel="alternate stylesheet" type="text/css" href="<?php echo base_url()?>assets/switcher/green.css" title="moogreen-color" media="screen" />
<link rel="alternate stylesheet" type="text/css" href="<?php echo base_url()?>assets/switcher/navy.css" title="moonavy-color" media="screen" />
<link rel="alternate stylesheet" type="text/css" href="<?php echo base_url()?>assets/switcher/orange.css" title="mooorange-color" media="screen" />
<link rel="alternate stylesheet" type="text/css" href="<?php echo base_url()?>assets/switcher/pink.css" title="moopink-color" media="screen" />
<link rel="alternate stylesheet" type="text/css" href="<?php echo base_url()?>assets/switcher/purple.css" title="moopurple-color" media="screen" />
<link rel="alternate stylesheet" type="text/css" href="<?php echo base_url()?>assets/switcher/red.css" title="moored-color" media="screen" />
<script type="text/javascript" src="<?php echo base_url()?>assets/switcher/switchstylesheet.js"></script>
<script type="text/javascript">
$(document).ready(function(){ 
	$(".changecolor").switchstylesheet( { seperator:"color"} );
});
</script>

