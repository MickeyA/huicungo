<!DOCTYPE html>
<html lang="en">

<?=$header?>

<body>
  <?=$navbar?>
  <!--/.navbar -->

  <?=$slider?>
  <!-- /.fullwidthbanner-container -->

  <?=$content?>

  <?=$footer?>

</body>

<!-- Mirrored from themes.iki-bir.com/moose/index12.html by HTTrack Website Copier/3.x [XR&CO'2014], Fri, 31 Jan 2020 21:27:00 GMT -->
</html>